import json
import os
import pipes
import urllib.parse
import socket
import re
import requests
from http.client import responses

import click

from joules.constants import DEFAULT_HEADERS
from joules.lib import check_missing_path, make_website, detect


@click.command()
@click.option(
    "-v",
    "--verbose",
    "verbose",
    help="Set to verbose mode",
    default=False,
    show_default=True,
    is_flag=True,
)
@click.option(
    "--skip-out",
    "skip_out",
    help="Skip writing Joules files",
    default=False,
    show_default=True,
    is_flag=True,
)
@click.option(
    "--project-name",
    "project_name",
    help="Set the name of the project",
    default=None,
)
@click.option(
    "--target-url",
    "target_url",
    help="Set the target URL for the project",
    default=None,
)
@click.option(
    "--config",
    "config_file_path",
    help="Set Joules config file",
    default=".joules",
    show_default=True,
)
def init(
    verbose=False,
    skip_out=False,
    project_name=None,
    target_url=None,
    config_file_path=".joules",
):
    """
    Initialize a Joules project.
    """

    click.secho("\n👋 Starting a new Joules project", fg="green")

    # Check if config file exists
    config_exists = os.path.exists(config_file_path)

    # If config exists throw error
    if config_exists:

        click.secho(
            f"❌ ERROR: Config file already exists at '{config_file_path}'", fg="red"
        )
        raise ValueError("Duplicate config file name found.")

    # Get project name if not provided
    if not project_name:
        project_name = click.prompt("What should we call this project?")

    # Request the target URL or IP address if not provided
    if not target_url:
        target_url = click.prompt("What is the target URL or IP?")

    # Parse the target URL
    parsed_url = urllib.parse.urlparse(target_url)

    # Check if the target is an IP
    if not re.match(r"[a-zA-Z]+", f"{parsed_url.netloc}{parsed_url.path}"):

        # Set the IP for the target
        target_ip = target_url

    else:

        # Validate URL protocol
        while parsed_url.scheme == "":

            click.secho(
                f"Invalid URL '{target_url}': No schema supplied. Perhaps you meant https://{target_url}?",
                fg="yellow",
            )

            # Request the target URL
            target_url = click.prompt(
                "What is the target URL?", default=f"https://{target_url}"
            )

            # Parse the target URL
            parsed_url = urllib.parse.urlparse(target_url)

        click.secho(f"Using {parsed_url.scheme.upper()} protocol", fg="green")

        # Get the URL port according to HTTP/HTTPS
        if parsed_url.scheme == "https":
            target_port = 443

        else:
            target_port = 80

        # Get the IP for the website
        target_ip = socket.getaddrinfo(parsed_url.netloc, target_port)[0][4][0]

    click.secho(f"Target IP determined: {target_ip}", fg="green")

    # Checking target URL
    click.secho("Sending request to target... ", nl=False)
    response = requests.get(target_url, headers=DEFAULT_HEADERS)
    status_color = "green" if response.status_code == 200 else "yellow"
    click.secho(
        f"{response.status_code} {responses[response.status_code]}", fg=status_color
    )

    click.secho("\n\n\n# Pre-process for path enumeration #", fg="magenta")

    ### Check URL redirection
    response = requests.get(target_url, allow_redirects=False, headers=DEFAULT_HEADERS)

    # Introduce a safety cut-off to make sure we don't loop forever
    cutoff = 10
    iterations = 0

    # Store all redirects
    redirects = [target_url]

    while response.status_code == 301:

        # Get next location from headers
        new_url = response.headers["Location"]

        # Store new redirect
        redirects.append(new_url)

        # Call new URL
        response = requests.get(new_url, allow_redirects=False, headers=DEFAULT_HEADERS)

        # Check the cutoff
        iterations += 1
        if iterations > cutoff:
            click.secho(
                f"❌ ERROR: Too many redirects calling URL '{target_url}'", fg="red"
            )
            raise ValueError(
                "Too many redirects while determining redirection location for '{target_url}'"
            )

    # Checking redirection results
    if len(redirects) > 1:

        target_url = redirects[-1]

        click.secho(f"\n👉 Target URL redirects to '{target_url}'", fg="red")

        redirect_chain = " -> ".join(redirects)
        click.secho(f"\t{redirect_chain}")
        click.secho(f"\tChanged target URL to '{target_url}'", fg="yellow")

    else:
        click.secho(
            f"\n👉 No redirects found, will use '{target_url}' as target", fg="green"
        )

    ### Check inexistent path
    missing_path = check_missing_path(parsed_url)

    ### Software detection
    click.secho("\n\n\n# Starting Software Detection #", fg="magenta")

    website = make_website(target_url)
    detect_results = detect(website)

    config = {
        "project": project_name,
        "target": target_url,
        "target_ip": target_ip,
        "detect_results": detect_results,
        "missing_path": missing_path,
    }

    env_file = f"""
export JOULES_PROJECT={(pipes.quote(str(project_name)))}
export JOULES_TARGET={(pipes.quote(str(target_url)))}
export JOULES_TARGET_IP={(pipes.quote(str(target_ip)))}
export JOULES_CONFIG={config_file_path}
    """

    if verbose:
        print("\n\n")
        print("Config:", json.dumps(config, indent=4))
        print("Env:", env_file)

    if not skip_out:
        with open(config_file_path, "w") as config_outfile:
            json.dump(config, config_outfile, indent=4)

        with open(config_file_path + ".env", "w") as env_outfile:
            env_outfile.write(env_file)

    print("\n\n")
