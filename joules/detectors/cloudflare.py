import click


def detect_cloudflare(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if "cloudflare" in website["server_header"].lower():
        found = True
        meta["confirmed_by"].append("Server Header")
        meta["header"] = website["server_header"]

        click.secho(f"\n👉 Website is using CloudFlare DNS routing", fg="green")
        click.secho(f"\t👉 Server: {website.get('server_header')}")

    return {"found": found, "meta": meta}
