import click


def detect_aws_s3(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if "amazons3" in website["server_header"].lower():
        found = True
        meta["confirmed_by"].append("Server Header")
        meta["header"] = website["server_header"]

        click.secho(f"\n👉 Website hosted using AWS S3", fg="green")
        click.secho(f"\t👉 Server: {website.get('server_header')}")

    return {"found": found, "meta": meta}
