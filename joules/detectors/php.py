import click


def detect_php(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    # Check the x-used header
    if "php" in website["x_used_header"].lower():
        found = True
        meta["confirmed_by"].append("x-used")
        meta["x-used"] = website["x_used_header"]

    # Check the X-Powered-By header
    if "php" in website["x_powered_by_header"].lower():
        found = True
        meta["confirmed_by"].append("X-Powered-By")
        meta["X-Powered-By"] = website["x_powered_by_header"]

    # Check if there is a Set-Cookie for PHPSESSID
    if "PHPSESSID" in website["cookies_headers"]:
        found = True
        meta["confirmed_by"].append("PHPSESSID")
        meta["PHPSESSID"] = website["cookies_headers"]

    if found:
        click.secho(f"\n👉 Website is using PHP", fg="green")
        detected = ", ".join(meta["confirmed_by"])
        click.secho(f"\tDetected by: {detected}")

    return {"found": found, "meta": meta}
