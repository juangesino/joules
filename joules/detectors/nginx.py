import click


def detect_nginx(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if "nginx" in website["server_header"].lower():
        found = True
        meta["confirmed_by"].append("Server Header")
        meta["header"] = website["server_header"]

        click.secho(f"\n👉 Website is using nginx as a reverse proxy server", fg="green")
        click.secho(f"\tServer: {website.get('server_header')}")

    return {"found": found, "meta": meta}
