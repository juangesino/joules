import click
import requests

from joules.constants import DEFAULT_HEADERS


def detect_wordpress(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    # Check X-Powered-By header
    if "WP Engine" in website["x_powered_by_header"].lower():
        found = True
        meta["confirmed_by"].append("X-Powered-By")

    # Make calls to site to determine if WordPress
    wp_base_url = website["url"] + ("" if website["url"][-1] == "/" else "/")

    # Check license.txt
    wp_license_url = wp_base_url + "license.txt"
    wp_response = requests.get(wp_license_url, headers=DEFAULT_HEADERS)
    if (
        wp_response.status_code == 200
        and "WordPress - Web publishing software" in wp_response.text
    ):
        found = True
        meta["confirmed_by"].append("license.txt")

    if found:
        click.secho(f"\n👉 Website is using WordPress", fg="green")
        detected = ", ".join(meta["confirmed_by"])
        click.secho(f"\tDetected by: {detected}")

    return {"found": found, "meta": meta}
