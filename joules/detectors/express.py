import click


def detect_expressjs(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if "express" in website["x_powered_by_header"].lower():
        found = True
        meta["confirmed_by"].append("X-Powered-By")
        meta["header"] = website["x_powered_by_header"]

        click.secho(f"\n👉 Website is using Express.js", fg="green")

    return {"found": found, "meta": meta}
