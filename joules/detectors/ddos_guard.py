import click


def detect_ddos_guard(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if "ddos-guard" in website["server_header"].lower():
        found = True
        meta["confirmed_by"].append("Server Header")
        meta["header"] = website["server_header"]

        click.secho(
            f"\n👉 Website is using ddos-guard for DDoS protection!", fg="yellow"
        )
        click.secho(f"\t👉 Server: {website.get('server_header')}")

    return {"found": found, "meta": meta}
