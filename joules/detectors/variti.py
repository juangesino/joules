import click


def detect_variti(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if website["x_variti_header"]:
        found = True
        meta["confirmed_by"].append("X-VARITI-CCR")
        meta["header"] = website["x_variti_header"]

        x_variti_header = website["x_variti_header"]
        click.secho(f"\n👉 Website is using variti.io for DDoS protection!", fg="yellow")
        click.secho(f"\t👉 X-VARITI-CCR: {x_variti_header}")

    return {"found": found, "meta": meta}
