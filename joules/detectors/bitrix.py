import click


def detect_bitrix(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if "bitrix" in website["x_powered_cms_header"].lower():
        found = True
        meta["confirmed_by"].append("x-powered-cms")
        meta["header"] = website["x_powered_cms_header"]

        click.secho(f"\n👉 Website is using Bitrix Site Manager", fg="green")

    return {"found": found, "meta": meta}
