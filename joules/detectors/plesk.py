import click


def detect_plesk(website):

    # Store response
    found = False
    meta = {"confirmed_by": []}

    if "plesklin" in website["x_powered_by_header"].lower():
        found = True
        meta["confirmed_by"].append("X-Powered-By")
        meta["header"] = website["x_powered_by_header"]

        click.secho(f"\n👉 Website is using PleskLin hosting management", fg="green")

    return {"found": found, "meta": meta}
