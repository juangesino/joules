import requests
from hashlib import sha256

import click

from joules.constants import DEFAULT_HEADERS
from joules.detectors.aws_s3 import detect_aws_s3
from joules.detectors.bitrix import detect_bitrix
from joules.detectors.cloudflare import detect_cloudflare
from joules.detectors.ddos_guard import detect_ddos_guard
from joules.detectors.express import detect_expressjs
from joules.detectors.nginx import detect_nginx
from joules.detectors.php import detect_php
from joules.detectors.plesk import detect_plesk
from joules.detectors.variti import detect_variti
from joules.detectors.wordpress import detect_wordpress


def check_missing_path(parsed_url):

    # Generate URL for missing path
    random_string = sha256(b"hello world").hexdigest()
    missing_url = f"{parsed_url.scheme}://{parsed_url.netloc}/{random_string}"

    # Send request to missing path
    response = requests.get(missing_url, headers=DEFAULT_HEADERS)

    # Check status code
    if response.status_code == 200:
        click.secho(f"\n👉 Missing path returns {response.status_code}", fg="red")
        click.secho(
            f"When running gobuster, you will need to filter by length. Run gobuster once and then use the given length for the `--exclude-length` option",
            fg="yellow",
        )
    elif response.status_code == 404:
        click.secho(f"\n👉 Missing path returns {response.status_code} 🎉", fg="green")

    else:
        click.secho(f"\n👉 Missing path returns {response.status_code}", fg="yellow")

    return {"status_code": response.status_code}


def make_website(target_url):
    response = requests.get(target_url, headers=DEFAULT_HEADERS)

    server_header = response.headers.get("Server", "")
    x_used_header = response.headers.get("x-used", "")
    x_powered_cms_header = response.headers.get("x-powered-cms", "")
    x_powered_by_header = response.headers.get("X-Powered-By", "")
    x_powered_by_header += response.headers.get("x-powered-by", "")
    cookies_headers = response.headers.get("Set-Cookie", "")
    x_variti_header = response.headers.get("X-VARITI-CCR", None)

    website = {
        "url": target_url,
        "headers": response.headers,
        "server_header": server_header,
        "x_used_header": x_used_header,
        "x_powered_cms_header": x_powered_cms_header,
        "x_powered_by_header": x_powered_by_header,
        "x_powered_by_header": x_powered_by_header,
        "cookies_headers": cookies_headers,
        "x_variti_header": x_variti_header,
    }

    return website


def detect(website):

    results = {
        "aws_s3": detect_aws_s3(website),
        "bitrix": detect_bitrix(website),
        "cloudflare": detect_cloudflare(website),
        "ddos_guard": detect_ddos_guard(website),
        "express": detect_expressjs(website),
        "nginx": detect_nginx(website),
        "php": detect_php(website),
        "plesk": detect_plesk(website),
        "variti": detect_variti(website),
        "wordpress": detect_wordpress(website),
    }

    return results
