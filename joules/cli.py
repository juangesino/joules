import click

from .config import commands as source

# from .checkpoint import commands as checkpoint
# from .clone import commands as clone
# from .docs import commands as docs


@click.group()
@click.version_option(prog_name="joules", message="%(prog)s v%(version)s")
def cli():
    """
    Welcome to Joules! 👋

    """
    pass


cli.add_command(source.init)
# cli.add_command(checkpoint.checkpoint)
# cli.add_command(clone.clone)
# cli.add_command(docs.docs)


if __name__ == "__main__":
    cli()
