from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

with open("requirements.txt", "r", encoding="utf-8") as fh:
    requirements = fh.read()

setup(
    name="joules",
    version="0.0.2",
    author="Juan Gesino",
    author_email="fief_tops.0n@icloud.com",
    license="",
    description="CLI for InfoSec projects",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/juangesino/joules/",
    packages=find_packages(),
    install_requires=[requirements],
    python_requires=">=3.8",
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent",
    ],
    entry_points={"console_scripts": ["joules = joules.cli:cli"]},
)
