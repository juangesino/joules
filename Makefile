.PHONY: help
.DEFAULT_GOAL := help


help:
	@grep -E --no-filename '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## Install the CLI
	pip install -e ./
	
upgrade: ## Upgrade the CLI
	pip install -e ./
